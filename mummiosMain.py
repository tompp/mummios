#!/usr/bin/python3.4
# -*- coding: utf-8 -*-

# TODO:
# Background image https://stackoverflow.com/questions/10639914/is-there-a-way-to-get-bings-photo-of-the-day from here Bing image of the day
# Aamulehti, Iltalehti and yle crawlers need to be made or just RSS
# Weather getter also to be built, https://yle.fi/saa/suomi/tampere/tampereen+keskusta/ ONE POSSIBILITY
# AL: https://www.aamulehti.fi/rss-syotteet/
# YLE:
    # Luetuimmat: https://feeds.yle.fi/uutiset/v1/mostRead/YLE_UUTISET.rss
    # Loput combo: https://yle.fi/uutiset/rss
# IL: http://www.iltalehti.fi/rss/

import sys
import codecs
import re
import urllib
import pprint as pp
import subprocess
import requests
import random
import time
import os

from PyQt5 import QtCore, QtGui, QtWidgets
from mummiosLayout import Ui_MainWindow
from threadClass import yleMostReadClass

class MummiosMain(Ui_MainWindow):
    """class for main window"""

    def __init__(self, MainWindow):
        Ui_MainWindow.__init__(self)
        self.setupUi(MainWindow)

        self.yleMostReadThread = yleMostReadClass()
        self.yleMostReadThread.dataSig.connect(self.yleMostRead)
        self.yleMostReadThread.ilSig.connect(self.ilMostRead)
        self.yleMostReadThread.start()

        ## Make the connections
        self.actionQuit.triggered.connect(QtWidgets.QApplication.quit)

    def yleMostRead(self, entries):
        for entry in entries:
            self.yleLuetuimmat.addWidget(QtWidgets.QLabel(entry[0]))

    def ilMostRead(self, entries):
        for entry in entries:
            self.ilLuetuimmat.addWidget(QtWidgets.QLabel(entry[0]))

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    prog = MummiosMain(MainWindow)
    MainWindow.show()
    sys.exit(app.exec())

