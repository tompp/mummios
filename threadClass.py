#!/usr/bin/python3.4
# -*- coding: utf-8 -*-

from PyQt5 import QtCore, QtGui, QtWidgets
import subprocess
import socket
import time
import requests
import zipfile
import io
import os
import fileinput
import sys
import feedparser

# Currently not in use !!
class CommunicatorClass(QtCore.QThread):

    # Create the signal to pass the text
    text = QtCore.pyqtSignal(str)

    def __init__(self, parent=None):
        super(CommunicatorClass, self).__init__(parent)
        self.message = "No message set"

    def run(self):
        self.text.emit(self.message)

    def setMessage(self, text):
        self.message = text

class yleMostReadClass(QtCore.QThread):

    # Create signal to pass data to the GUI
    dataSig = QtCore.pyqtSignal(list)
    ilSig = QtCore.pyqtSignal(list)



    def __init__(self, parent=None):
        super(yleMostReadClass, self).__init__(parent)
        self.rssurl = "https://feeds.yle.fi/uutiset/v1/mostRead/YLE_UUTISET.rss"
        self.ilrssurl = "http://www.iltalehti.fi/rss/uutiset.xml"

    def run(self):
        rawrss = feedparser.parse(self.rssurl)
        ilrawrss = feedparser.parse(self.ilrssurl)
        unpackedRssList = self.unpackRss(rawrss)
        unpackedilssList = self.unpackIlRss(ilrawrss)
        self.dataSig.emit(unpackedRssList)
        self.ilSig.emit(unpackedilssList)

    def unpackRss(self, rawrss):
        entries = [x for x in rawrss['entries']]
        entrylink = []
        for idx in range(10):
            entrylink.append((entries[idx]['title'], entries[idx]['link']))
        return entrylink

    def unpackIlRss(self, ilrawrss):
        entrylink = []
        for idx in range(10):
            entrylink.append((ilrawrss['entries'][idx]['title'], ilrawrss['entries'][idx]['link']))
        return entrylink


